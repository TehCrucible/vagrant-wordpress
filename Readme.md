#vagrant-wordpress

### Vagrant Install

1.  This box uses Ubuntu 14.04 LTS "Trustry Thar" 64bit, so if you don't have the basic box already, do a
    `vagrant box add ubuntu/trusty64` before.
2.  `git clone https://TehCrucible@bitbucket.org/TehCrucible/vagrant-wordpress.git`
3.  Edit `wp-config-dev.php` to suit your development and production environments
4.  `vagrant up`